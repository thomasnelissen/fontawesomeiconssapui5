/* global be:true */
/* global _:true */
sap.ui.define([
		"sap/ui/core/IconPool",
		"./util/lodash.min"
	], // library dependency
	function(IconPool, lodashjs) {
		"use strict";

		var fixedHex = function(number, length) {
			var str = number.toString(16).toUpperCase();
			while (str.length < length) {
				str = "0" + str;
			}
			return str;
		};

		sap.ui.getCore().initLibrary({
			name: "be.kbc.icons",
			version: "1.0.0",
			dependencies: ["sap.ui.core"],
			types: [],
			interfaces: [],
			elements: []
		});

		var path = jQuery.sap.getResourcePath("be/kbc/icons");
		jQuery.sap.includeStyleSheet(path + "/css/font-awesome.css", "facss", function() {

			var styleSheet = _.find(document.styleSheets, function(sheet) {
				if (!sheet.href){
					return false;
				}
				return sheet.href.indexOf("font-awesome") !== -1;
			});

			var classes = [];
			_.forEach(styleSheet.cssRules, function(rule) {
				if (rule.selectorText && rule.selectorText.indexOf(".fa-") === 0 && rule.selectorText.indexOf("::before") > 0) {
					var style = rule.style.content;

					if (style.indexOf("\\") === 1) {
						style = parseInt(style.substring(2, style.length - 1), 16);
					} else {
						style = style[1].charCodeAt(0);
					}
					var iconCode = fixedHex(style, 4);
					_.forEach(rule.selectorText.split(","), function(value) {
						var name = value.replace("::before", "").trim().substring(1).replace("fa-", "");
						classes.push({
							"name": name,
							"code": iconCode
						});
					});
				}
			});

			_.forEach(classes, function(cssClass) {
				IconPool.addIcon(cssClass.name, "fa", "fontawesome", cssClass.code);
			});
		});

		return be.kbc.icons;

	}, /* bExport= */ false);